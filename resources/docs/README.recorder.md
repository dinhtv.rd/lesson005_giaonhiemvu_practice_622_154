# Document for making a Recorder

## Recorder

The job of a recorder is to process and store context information in the conversation with each `conversation_id`

## Files

* Recorder: `src/utils/recorder.py`
* Classes of Records in a Recorder: `src/utils/records.py`

## Default variables and methods

* Recorder:
    * `self.sender_id = sender_id`: conversation_id of user
    * `self.state = True`: state of Bot (Chatting or Listening)
    * `self.old_action = None`: previous of previous action
    * `self.pre_action = None`: previous action
    * `self.cur_action = None`: current action
    * `self.cur_intent = None`: current intent
    * `self.last_message = None`: last message from user
    * `self.get_last_message()`: get last message from user
    * `self.get_last_slot_receive(name_slot)`: get last slot receive by name
    * `self.get_last_slot_value(name_slot)`: get last slot not None by name
    * `self.get_current_record()`: get dict of values of record
    * `self.get_user_info_record()`: get dict of information of user from DB

* Each extended class of `ImplRecord` or `ImplRecordAsync`:
    * `self.record`: value of a `Record` in the `Recorder`, the type of `self.record` must be normal data type (not be
      an object) to help dump and store data with redis
    * `self.update()`: should be change `self.record` with context of conversation
    * `self.get_value()`: `return self.record` (fixed)

## Define

* `records_init`: define records as a dict, each item is a record belong to class, which extend `ImplRecord`
  or `ImplRecordAsync`
  in `records.py`. The records in `records_init` can be using as contexts in [Flow intent](README.flow.md), as
  properties in [Template action](README.template.md).
* `query_init`: define list of record in `records_init`, which are the records with async update function. This is
  consistent with records that need to wait for a response from other connections

```python
records_init = {
    "count_action": CountAction(),
    "check_repeat": CheckActionRepeat(),
    "get_pre_intent": GetPreIntent(),
    "get_pre_action": GetPreAction(),
    "get_cur_intent": GetCurrentIntent(),
    "flow_repeat_four_times": FlowRepeatFourTimes(),

    "get_report": GetReport()
}
query_init = [
]
```

## Coding

* Each record in `records_init` should be coded with an `update` function to update information throughout the
  conversation.
* The class should be defined in `src/utils/records.py` and implements class `ImplRecord` or `ImplRecordAsync`.
* The `update()` function should be updated the value of `self.record` based context of conversation
* These records can communicate with each other. Note the order of records.
* Code example:

```python

class ImplRecord:
    def __init__(self):
        self.record = None

    def update(self, recorder) -> None:
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :return: None
        """
        raise NotImplementedError("An record must implement its update method")

    def reset(self) -> None:
        """
        reset value of item record when recorder run reset
        :return: None
        """
        self.record = None

    def set_record(self, record):
        self.record = record

    def get_value(self):
        return self.record


class ImplRecordAsync:
    def __init__(self):
        self.record = None

    async def update(self, recorder) -> None:
        """
        check_update and update from values of recorder
        :param recorder: Recorder
        :return: None
        """
        raise NotImplementedError("An record must implement its update method")

    def reset(self) -> None:
        """
        reset value of item record when recorder run reset
        :return: None
        """
        self.record = None

    def set_record(self, record):
        self.record = record

    def get_value(self):
        return self.record


class CheckActionRepeat(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = False

    def update(self, recorder) -> None:
        pre_action = recorder.pre_action
        cur_action = recorder.cur_action
        self.record = pre_action == cur_action


class GetCurrentIntent(ImplRecord):
    def __init__(self):
        super().__init__()
        self.record = None

    def update(self, recorder) -> None:
        self.record = recorder.cur_intent

```  

* etc.