import json
import logging

from config import (
    ENDPOINT_DATABASE,
    DB_TEST_MODE,
    DB_TEST_SENDER_ID,
    DEFAULT_USER_INFO
)
from src.core.database.database_connection import DatabaseConnection
from src.dev.preprocessors.message.vietnamese_norm import norm_text

logger = logging.getLogger(__name__)


class UserInfoStore:
    def __init__(self):
        self.connect = DatabaseConnection(host=ENDPOINT_DATABASE.get("host"),
                                          username=ENDPOINT_DATABASE.get("username"),
                                          password=ENDPOINT_DATABASE.get("password"),
                                          db=ENDPOINT_DATABASE.get("db"),
                                          port=ENDPOINT_DATABASE.get("port"))
        self.user_information_table = ENDPOINT_DATABASE.get("table_user_information")
        self.config_user_info = DEFAULT_USER_INFO

        self.PARAMS_INT = []
        self.PARAMS_STR = []
        self.PARAMS_ARR = []
        self.PARAMS_BOOL = []
        self.PARAMS_FLOAT = []
        for key in self.config_user_info:
            value = self.config_user_info.get(key)
            if isinstance(value, int):
                self.PARAMS_INT.append(key)
            elif isinstance(value, float):
                self.PARAMS_FLOAT.append(key)
            elif isinstance(value, str):
                self.PARAMS_STR.append(key)
            elif isinstance(value, list):
                self.PARAMS_ARR.append(key)
            elif isinstance(value, bool):
                self.PARAMS_BOOL.append(key)

    def get(self, sender_id):
        if DB_TEST_MODE:
            sender_id = DB_TEST_SENDER_ID
        if sender_id is None:
            return {}
        list_slots = [key for key in self.config_user_info]
        keyword = ",".join(list_slots)
        sql = f"SELECT {keyword} FROM "
        sql += f"{self.user_information_table}" + " WHERE id_conversation = %s"
        connection = self.connect.get_connection()
        cursor = connection.cursor()
        connection.commit()
        cursor.execute(sql, [sender_id])
        query_result = cursor.fetchall()
        cursor.close()
        connection.close()
        answer = {}
        if query_result is not None and len(query_result) > 0:
            for x in query_result:
                for i, name in enumerate(list_slots):
                    answer[name] = {}
                    if name in self.PARAMS_ARR:
                        answer[name] = self.process_array_param(x[i])
                    elif name in self.PARAMS_INT:
                        answer[name] = self.process_int_param(x[i])
                    elif name in self.PARAMS_FLOAT:
                        answer[name] = self.process_float_param(x[i])
                    elif name in self.PARAMS_BOOL:
                        answer[name] = self.process_bool_param(x[i])
                    else:
                        answer[name] = self.process_string_param(x[i])
        answer = self.normalize_params(answer)
        logger.info(f"[USER INFO][{sender_id}] - Get information of user {sender_id} successful")
        return answer

    @staticmethod
    def process_array_param(text):
        try:
            text = f"{text}"
            text = text \
                .replace("\\", "") \
                .replace("\"", "") \
                .replace("'", "") \
                .replace(" ,", ",") \
                .replace(", ", ",") \
                .replace("[", "[\"") \
                .replace("]", "\"]") \
                .replace(",", "\",\"")
            arr_item = json.loads(text)
            arr_item = [norm_text(x) for x in arr_item]
            return arr_item
        except Exception as e:
            logger.error(f"Process array param error: {e}")
            return None

    @staticmethod
    def process_int_param(x):
        try:
            int_item = int(x)
        except Exception as e:
            logger.error(f"Process int param error: {e}")
            int_item = None
        return int_item

    @staticmethod
    def process_float_param(x):
        try:
            int_item = float(x)
        except Exception as e:
            logger.error(f"Process float param error: {e}")
            int_item = None
        return int_item

    @staticmethod
    def process_bool_param(x):
        bool_item = None
        if x == "true":
            bool_item = True
        elif x == "false":
            bool_item = False
        return bool_item

    @staticmethod
    def process_string_param(x):
        x = norm_text(str(x).strip())
        return x

    @staticmethod
    def normalize_params(answer):
        return answer

    def update_report(self, sender_id, report):
        sql = f"UPDATE {self.user_information_table}"
        sql += " SET report=%s WHERE id_conversation = %s"
        connection = self.connect.get_connection()
        cursor = connection.cursor()
        cursor.execute(sql, [report, sender_id])
        connection.commit()
        cursor.close()
        connection.close()
        logger.info(f"[USER INFO][{sender_id}] - Update report to DB: {report}")
