import json
import re
import os
import yaml
import copy, html

from config import PATH_CONFIG_USER_INFO, DOMAIN_START_ACTIONS
from src.core.initializer.html_remover import remove_html
from src.core.initializer.xml2dict import xml2dict_from_file, beautify_dict2xml
from src.core.initializer.form_matching import match_style, text2style
from src.core.utils.classes.class_normalization import norm_name_class
from src.core.utils.io import jsonio

from config_platform import (
    PATH_DETERMINER,
    PATH_VALIDATE_FORMAT,
    PATH_CONNECTION_RULE,
    PATH_NODES_DUMPED,
    PATH_EDGES_DUMPED,
    PATH_EDGES_FULL_DUMPED,
    PATH_CONDITIONS_DUMPED,
    DOMAIN_MANUAL_CONFIG,
    DOMAIN_AUTO_CONFIG,
    PATH_DOMAIN,
    PATH_ACTIONS_PY,
    DIR_TEMPLATE,
    DOMAIN_ACTIONS,
    DOMAIN_INTENTS,
    VERTICES,
    FLOWS,
    PATH_API_METADATA_DUMPED,
    PATH_GRAPH_DUMPED, DOMAIN_FINISH_ACTIONS,
)


class DrawioExtractor:
    def dict2xml(self):
        nodes = self.get_nodes()
        conditions = self.get_conditions()
        conditions_cell_id = {}
        text_cell_id = {}
        for node_id in nodes:
            node = nodes.get(node_id)
            type_node = node.get("type_node")
            if type_node in ["bilingual_action", "english_action"]:
                params = node.get("params")
                for key in params:
                    value = params.get(key)[0]
                    if "|" in value:
                        tmp = value.split("|")
                        text_action = tmp[0]
                        text_id = tmp[1]
                        text_cell_id.update({
                            text_id: text_action
                        })

        for condition_id in conditions:
            condition = conditions.get(condition_id)
            condition_desc = condition.get("desc")
            condition_type = condition.get("type")
            if condition_type in ["check", "update_local", "update_global"]:
                tmp = condition_desc.split("|")
                condition_value = tmp[0]
                condition_cell_id = tmp[1]
                conditions_cell_id.update({
                    condition_cell_id: condition_value
                })

        for idx, mx_cell in enumerate(self.list_mx_cell):
            id_cell = mx_cell.get("id")
            node = nodes.get(id_cell)
            if node and node.get("type_node") not in ["destination_node",
                                                      "shortcut_node",
                                                      "start_node",
                                                      "end_node",
                                                      "fixed_condition",
                                                      "correction_node",
                                                      "user_information"
                                                      ]:
                self.list_mx_cell[idx]["value"] = node.get("desc")
            condition = conditions_cell_id.get(id_cell)
            if condition:
                self.list_mx_cell[idx]["value"] = condition
            text_action = text_cell_id.get(id_cell)
            if text_action:
                self.list_mx_cell[idx]["value"] = text_action
        xml_node = {
            "root": {
                "mxCell": self.list_mx_cell
            }
        }
        for key in self.xml2dict:
            if key != "root":
                xml_node.update({
                    key: self.xml2dict.get(key)
                })
        dom = beautify_dict2xml(xml_node, "mxGraphModel")
        with open(PATH_GRAPH_DUMPED, "w") as fileout:
            fileout.write(dom)

    def __init__(self, path_xml_drawio):
        self.CONDITION_FLOW = "condition_flow"
        self.UPDATE_LOCAL = "update_local"
        xml2dict = xml2dict_from_file(path_xml_drawio)
        self.xml2dict = xml2dict
        list_mx_cell = xml2dict.get("root").get("mxCell")
        self.list_mx_cell = copy.deepcopy(list_mx_cell)
        self.dict_mx_cell = {}
        for mx_cell in list_mx_cell:
            id_cell = mx_cell.get("id")
            self.dict_mx_cell[id_cell] = mx_cell
            self.dict_mx_cell[id_cell]["child"] = []
        for child_cell in list_mx_cell:
            if "parent" in child_cell:
                id_child = child_cell.get("id")
                id_parent = child_cell.get("parent")
                if id_parent in self.dict_mx_cell:
                    if "child" not in self.dict_mx_cell[id_parent]:
                        self.dict_mx_cell[id_parent]["child"] = []
                    self.dict_mx_cell[id_parent]["child"].append(id_child)
        with open(PATH_DETERMINER, "r") as file_in:
            self.determiner = jsonio.load_utf8(file_in)
            self.determiner_table_row = self.determiner.get("table_row")

        self.data = {}

        self.data["bilingual_action"] = self.bilingual_actions = self.extract_node("dict", "bilingual_action")
        self.data["english_action"] = self.english_actions = self.extract_node("dict", "english_action")
        self.data["condition_flow"] = self.condition_flows = self.extract_node("dict", "condition_flow")
        self.data["intent_flow"] = self.intent_flows = self.extract_node("list", "intent_flow")
        self.data["update_local"] = self.update_local_flows = self.extract_node("list", "update_local")
        self.data["fixed_condition"] = self.fixed_conditions = self.extract_node(None, "fixed_condition")
        self.data["start_node"] = self.start_nodes = self.extract_node(None, "start_node")
        self.data["end_node"] = self.end_nodes = self.extract_node(None, "end_node")
        self.data["destination_node"] = self.destination_nodes = self.extract_node(None, "destination_node")
        self.data["shortcut_node"] = self.shortcut_nodes = self.extract_node(None, "shortcut_node")
        self.data["correction_node"] = self.correction_nodes = self.extract_node(None, "correction_node")
        self.data["api_node"] = self.api_nodes = self.extract_node(None, "api_node")
        self.data["user_information"] = self.user_information = self.extract_node("dict", "user_information")
        self.data["notification_node"] = self.notification_nodes = self.extract_node(None, "notification_node")

        self.conditions = {}

        nodes = {}
        for type_node in self.data:
            nodes.update(self.data.get(type_node))

        self.data["nodes"] = nodes
        self.data["edges"] = self.edges = self.extract_edges()

        self.validator = DrawioValidator(self)

        self.edges_full = []
        self.make_edge_full()

    def find_parent(self, node_id, node_type):
        if node_type is not None:
            return node_id, node_type
        if self.dict_mx_cell.get(node_id).get("parent"):
            parent_id = self.dict_mx_cell.get(node_id).get("parent")
            parent_type = self.get_nodes().get(parent_id).get("type_node")
            return self.find_parent(parent_id, parent_type)
        return node_id, node_type

    def extract_edges(self):
        edges = {}
        for cell_id in self.dict_mx_cell:
            cell = self.dict_mx_cell.get(cell_id)
            if "edge" in cell:
                source = cell.get("source")
                target = cell.get("target")
                if source:
                    source_type = self.dict_mx_cell.get(source).get("type_node")
                    source, source_type = self.find_parent(source, source_type)
                else:
                    source_type = None
                if target:
                    target_type = self.dict_mx_cell.get(target).get("type_node")
                    target, target_type = self.find_parent(target, target_type)
                else:
                    target_type = None
                edge = {
                    cell_id: {
                        "id": cell_id,
                        "source": source,
                        "target": target,
                        "source_type": source_type,
                        "target_type": target_type
                    }
                }
                edges.update(edge)
        return edges

    def extract_dict_row(self, parent_cell, type_node):
        params = {}
        for row_id in parent_cell.get("child"):
            row_cell = self.dict_mx_cell.get(row_id)
            style_cell = text2style(row_cell.get("style"))
            if match_style(style_cell, self.determiner_table_row):
                col_ids = row_cell.get("child")
                key = None
                value = None
                if len(col_ids) == 2:
                    for col_id in col_ids:
                        col_cell = self.dict_mx_cell.get(col_id)
                        if col_cell.get("mxGeometry") and col_cell.get("mxGeometry").get("x"):
                            if type_node in ["condition_flow", "bilingual_action", "english_action"]:
                                value = f'{remove_html(col_cell.get("value"))}|{col_id}'
                            else:
                                value = remove_html(col_cell.get("value"))
                        else:
                            key = remove_html(col_cell.get("value"))
                if key not in params:
                    params.update({
                        key: [value]
                    })
                else:
                    params[key].append(value)
        return params

    def extract_list_row(self, parent_cell, type_node):
        params = []
        for row_id in parent_cell.get("child"):
            row_cell = self.dict_mx_cell.get(row_id)
            style_cell = text2style(row_cell.get("style"))
            if match_style(style_cell, self.determiner_table_row):
                col_ids = row_cell.get("child")
                if len(col_ids) == 1:
                    col_cell = self.dict_mx_cell.get(col_ids[0])
                    if type_node in ["update_local"]:
                        value = f'{remove_html(col_cell.get("value"))}|{col_ids[0]}'
                    else:
                        value = remove_html(col_cell.get("value"))
                    params.append(value)
        return params

    @staticmethod
    def extract_connection_id(desc):
        connection_id = None
        pattern_connection_id = r"^(\d+\.){1,}"
        pattern = re.compile(pattern_connection_id)
        for match in pattern.finditer(desc):
            value = match.group()
            if not connection_id or len(value) > len(connection_id):
                connection_id = value
        return connection_id

    def extract_node(self, params_type, type_node):
        extracted_dict = {}
        for parent_id in self.dict_mx_cell:
            parent_cell = self.dict_mx_cell.get(parent_id)
            style_cell = text2style(parent_cell.get("style"))
            if match_style(style_cell, self.determiner.get(type_node)):
                desc = remove_html(parent_cell.get("value"))
                if params_type == "dict":
                    params = self.extract_dict_row(parent_cell, type_node)
                elif params_type == "list":
                    params = self.extract_list_row(parent_cell, type_node)
                else:
                    params = None

                connection_id = None
                if type_node in ["destination_node", "shortcut_node"]:
                    connection_id = self.extract_connection_id(desc)

                node_cell = {
                    parent_id: {
                        "id": parent_id,
                        "desc": desc,
                        "type_node": type_node
                    }
                }
                if connection_id is not None:
                    node_cell[parent_id]["connection_id"] = connection_id
                if params is not None:
                    node_cell[parent_id]["params"] = params
                extracted_dict.update(node_cell)
                assert "type_node" not in self.dict_mx_cell[parent_id] or \
                       self.dict_mx_cell[parent_id]["type_node"] == type_node, \
                    f"Node {parent_id}: 2 types exist ({self.dict_mx_cell[parent_id]['type_node']}, {type_node})"
                self.dict_mx_cell[parent_id]["type_node"] = type_node
        return extracted_dict

    def validate(self):
        return self.validator.validate()

    def get_nodes_by_type(self, type_node):
        return self.data[type_node]

    def get_nodes(self):
        return self.data["nodes"]

    def get_edges(self):
        return self.data["edges"]

    def get_node_by_id(self, node_id):
        return self.get_nodes().get(node_id)

    def get_type_of_node(self, node_id):
        if not self.get_node_by_id(node_id):
            return None
        return self.get_node_by_id(node_id).get("type_node")

    @staticmethod
    def index_desc(count, desc, map_count):
        if not re.search(r"^\[\d+\]", desc):
            count += 1
            while str(count) in map_count:
                count += 1
            desc = f"[{count}] {desc.strip()}"
            return count, desc
        return count, desc

    @staticmethod
    def index_text(count, text, map_count):
        is_open = False
        text_new = ""
        for c in text:
            if c == "{":
                is_open = True
                desc = ""
            if is_open:
                desc += c
            else:
                text_new += c
            if c == "}":
                is_open = False
                count, desc = DrawioExtractor.index_desc(count, desc[1:-1], map_count)
                text_new += "{" + f"{desc}" + "}"
        return count, text_new

    def index_nodes(self):
        nodes = self.data["nodes"]
        count = 0
        map_count = {}

        # get avail condition_id
        for node_id in nodes:
            node = nodes.get(node_id)
            desc = node.get("desc")
            type_node = node.get("type_node")
            if desc is not None:
                count_tmp = self.extract_condition_code(desc)
                map_count[count_tmp] = True

            if type_node == self.CONDITION_FLOW:
                params = node.get("params")
                if params:
                    check_list = params.get("check")
                    update_list = params.get("update")
                    if check_list:
                        for idx, desc in enumerate(check_list):
                            if desc is not None:
                                count_tmp = self.extract_condition_code(desc)
                                map_count[count_tmp] = True
                    if update_list:
                        for idx, desc in enumerate(update_list):
                            if desc is not None:
                                count_tmp = self.extract_condition_code(desc)
                                map_count[count_tmp] = True
                    # index update_local node
            if type_node == self.UPDATE_LOCAL:
                params = nodes.get(node_id).get("params")
                if params:
                    for idx, desc in enumerate(params):
                        if desc is not None:
                            count_tmp = self.extract_condition_code(desc)
                            map_count[count_tmp] = True

            # index slot action
            if nodes.get(node_id).get("type_node") in ["bilingual_action", "english_action"]:
                params = nodes.get(node_id).get("params")
                if params:
                    for key in params:
                        value = params.get(key)
                        for idx, text in enumerate(value):
                            slots = self.get_slots_from_text(text)
                            for slot in slots:
                                count_tmp = self.extract_condition_code(slot)
                                map_count[count_tmp] = True

        for node_id in nodes:
            # index parent node by desc
            desc = nodes.get(node_id).get("desc")
            if desc is not None:
                count, nodes[node_id]["desc"] = self.index_desc(count, desc, map_count)

            # index condition_flow node
            if nodes.get(node_id).get("type_node") == self.CONDITION_FLOW:
                params = nodes.get(node_id).get("params")
                if params:
                    check_list = params.get("check")
                    update_list = params.get("update")
                    if check_list:
                        for idx, desc in enumerate(check_list):
                            if desc is not None:
                                count, nodes[node_id]["params"]["check"][idx] = self.index_desc(count, desc, map_count)
                    if update_list:
                        for idx, desc in enumerate(update_list):
                            if desc is not None:
                                count, nodes[node_id]["params"]["update"][idx] = self.index_desc(count, desc, map_count)

            # index update_local node
            if nodes.get(node_id).get("type_node") == self.UPDATE_LOCAL:
                params = nodes.get(node_id).get("params")
                if params:
                    for idx, desc in enumerate(params):
                        if desc is not None:
                            count, nodes[node_id]["params"][idx] = self.index_desc(count, desc, map_count)

            # index slot action
            if nodes.get(node_id).get("type_node") in ["bilingual_action", "english_action"]:
                params = nodes.get(node_id).get("params")
                if params:
                    for key in params:
                        value = params.get(key)
                        for idx, text in enumerate(value):
                            count, nodes[node_id]["params"][key][idx] = self.index_text(count, text, map_count)

        self.data["nodes"] = nodes.copy()

    @staticmethod
    def extract_condition_code(desc_tmp):
        condition_code = "unk"
        pattern_condition_code = r"^\[\d+\]"
        pattern = re.compile(pattern_condition_code)
        for match in pattern.finditer(desc_tmp):
            value = match.group()[1:-1]
            if condition_code == "unk" or len(value) > len(condition_code):
                condition_code = value
        return condition_code

    def init_condition(self):

        nodes = self.get_nodes()
        for node_id in nodes:
            node = nodes.get(node_id)
            type_node = node.get("type_node")

            # index condition_flow node
            if type_node == self.CONDITION_FLOW:
                params = node.get("params")
                if params:
                    check_list = params.get("check")
                    update_list = params.get("update")
                    if check_list:
                        for desc in check_list:
                            if desc is not None:
                                idx_code = self.extract_condition_code(desc)
                                self.conditions.update({
                                    idx_code: {
                                        "id": idx_code,
                                        "desc": desc,
                                        "type": "check"
                                    }
                                })
                    if update_list:
                        for desc in update_list:
                            if desc is not None:
                                idx_code = self.extract_condition_code(desc)
                                self.conditions.update({
                                    idx_code: {
                                        "id": idx_code,
                                        "desc": desc,
                                        "type": "update_global"
                                    }
                                })

            # index update_local node
            if type_node == self.UPDATE_LOCAL:
                params = node.get("params")
                if params:
                    for desc in params:
                        if desc is not None:
                            idx_code = self.extract_condition_code(desc)
                            self.conditions.update({
                                idx_code: {
                                    "id": idx_code,
                                    "desc": desc,
                                    "type": "update_local"
                                }
                            })

            # slot action
            if type_node in ["bilingual_action", "english_action"]:
                params = nodes.get(node_id).get("params")
                if params:
                    for key in params:
                        value = params.get(key)
                        for text in value:
                            slots = self.get_slots_from_text(text)
                            for slot_desc in slots:
                                idx_code = self.extract_condition_code(slot_desc)
                                self.conditions.update({
                                    idx_code: {
                                        "id": idx_code,
                                        "desc": slot_desc,
                                        "type": "update_slot"
                                    }
                                })

    @staticmethod
    def get_slots_from_text(text):
        is_open = False
        slots = []
        for c in text:
            if c == "{":
                is_open = True
                desc = ""
            if is_open:
                desc += c
            if c == "}":
                is_open = False
                slots.append(desc[1:-1])
        return slots

    def get_user_info(self):
        user_info = {}
        for user_info_id in self.user_information:
            user_info_node = self.user_information.get(user_info_id)
            user_info.update(user_info_node.get("params"))
        for key in user_info:
            value = user_info.get(key)
            value = value[0] if value else None
            user_info[key] = value
        user_info.update({
            "report": ""
        })
        return user_info

    def get_api_nodes(self):
        api_nodes = {}
        nodes = self.get_nodes()
        for node_id in nodes:
            node = nodes.get(node_id)
            type_node = node.get("type_node")
            if type_node == "api_node":
                api_nodes.update(node)
        return api_nodes

    def remove_id_nodes(self):
        for node_id in self.data["nodes"]:
            # remove desc of condition
            if self.data["nodes"][node_id]["type_node"] in ["update_local"]:
                for idx, param in enumerate(self.data["nodes"][node_id]["params"]):
                    tmp = self.data["nodes"][node_id]["params"][idx].split("|")[0]
                    self.data["nodes"][node_id]["params"][idx] = tmp

            if self.data["nodes"][node_id]["type_node"] in ["condition_flow"]:
                for key in self.data["nodes"][node_id]["params"]:
                    tmp = self.data["nodes"][node_id]["params"][key][0].split("|")[0]
                    self.data["nodes"][node_id]["params"][key][0] = tmp

            if self.data["nodes"][node_id]["type_node"] in ["bilingual_action", "english_action"]:
                for key in self.data["nodes"][node_id]["params"]:
                    tmp = self.data["nodes"][node_id]["params"][key][0].split("|")[0]
                    tmp = html.unescape(tmp)
                    tmp = tmp.replace("\\n", "\n")
                    self.data["nodes"][node_id]["params"][key][0] = tmp

    def dump_files(self):
        self.index_nodes()
        self.init_condition()
        self.dict2xml()
        self.remove_id_nodes()
        with open(PATH_API_METADATA_DUMPED, "w") as file_out:
            json.dump(self.get_api_nodes(), file_out, ensure_ascii=False, indent=2)
        with open(PATH_CONFIG_USER_INFO, "w") as file_out:
            json.dump(self.get_user_info(), file_out, ensure_ascii=False, indent=2)
        with open(PATH_NODES_DUMPED, "w") as file_out:
            json.dump(self.get_nodes(), file_out, ensure_ascii=False, indent=2)
        with open(PATH_EDGES_DUMPED, "w") as file_out:
            json.dump(self.get_edges(), file_out, ensure_ascii=False, indent=2)
        with open(PATH_EDGES_FULL_DUMPED, "w") as file_out:
            json.dump(self.get_edges_full(), file_out, ensure_ascii=False, indent=2)
        with open(PATH_CONDITIONS_DUMPED, "w") as file_out:
            json.dump(self.get_conditions(), file_out, ensure_ascii=False, indent=2)
        self.init_domain()

    def init_domain(self):
        domain_yml = {}
        domain_yml.update(DOMAIN_MANUAL_CONFIG)
        domain_yml.update(DOMAIN_AUTO_CONFIG)

        if os.path.isfile(PATH_DOMAIN):
            with open(os.path.join(PATH_DOMAIN), "r") as file_domain:
                try:
                    old_domain_yml = yaml.safe_load(file_domain)
                    for key in old_domain_yml:
                        if key in DOMAIN_MANUAL_CONFIG:
                            domain_yml[key] = old_domain_yml[key]
                except Exception as e:
                    print(f"Exception {e}")
                    pass
        list_intents = []
        list_actions = []
        list_finish_actions = []
        list_start_actions = []
        with open(PATH_DOMAIN, "w") as file_domain, \
                open(PATH_ACTIONS_PY, "w") as file_action, \
                open(os.path.join(DIR_TEMPLATE, "action_class.template"), "r") as file_class_template, \
                open(os.path.join(DIR_TEMPLATE, "action_py.template"), "r") as file_action_template:

            nodes = self.get_nodes()
            for node_id in nodes:
                node = nodes.get(node_id)
                type_node = node.get("type_node")
                if type_node == "intent_flow":
                    list_intents.extend(node.get("params"))
                if type_node in ["bilingual_action", "english_action", "correction_node", "api_node",
                                 "start_node", "end_node"]:
                    list_actions.append(node_id)
                    if type_node in ["end_node"]:
                        list_finish_actions.append(node_id)
                    if type_node in ["start_node"]:
                        list_start_actions.append(node_id)

            list_intents.sort()
            for intent in list_intents:
                if intent not in domain_yml[DOMAIN_INTENTS]:
                    domain_yml[DOMAIN_INTENTS].append(intent)
            for action in list_finish_actions:
                if action not in domain_yml[DOMAIN_FINISH_ACTIONS]:
                    domain_yml[DOMAIN_FINISH_ACTIONS].append(action)
            for action in list_start_actions:
                if action not in domain_yml[DOMAIN_START_ACTIONS]:
                    domain_yml[DOMAIN_START_ACTIONS].append(action)

            list_actions.sort()
            list_actions.append("action_quit")

            content = file_action_template.read()
            file_action.write(content)

            lines = file_class_template.readlines()
            for action in list_actions:
                if action not in domain_yml[DOMAIN_ACTIONS]:
                    domain_yml[DOMAIN_ACTIONS].append(action)
                    for line in lines:
                        if "ACTION_NAME" in line:
                            line = line.replace("ACTION_NAME", action)
                        if "ACTION_CLASS_NAME" in line:
                            line = line.replace("ACTION_CLASS_NAME", norm_name_class(action))
                        file_action.write(line)
                    file_action.write("\n\n\n")

            print("\n\nDOMAIN: ")
            print(yaml.dump(domain_yml, default_flow_style=False, sort_keys=False))
            yaml.dump(domain_yml, file_domain, default_flow_style=False, sort_keys=False)

    def get_conditions(self):
        return self.conditions

    def get_edges_full(self):
        return self.edges_full

    def make_edge_full(self):

        def connect_to(target_id):
            if not target_id:
                return [None]
            target_nodes_tmp = []

            if self.get_type_of_node(target_id) not in ["destination_node", "shortcut_node"]:
                return [target_id]
            elif self.get_type_of_node(target_id) == "shortcut_node":
                destination_id = self.get_destination_id(target_id)
                return connect_to(destination_id)
            else:
                for edge_id_tmp in self.get_edges():
                    edge_tmp = self.get_edges().get(edge_id_tmp)
                    if edge_tmp.get("source") == target_id:
                        new_target = edge_tmp.get("target")
                        target_nodes_tmp.extend(connect_to(new_target))
            return target_nodes_tmp

        edges = self.get_edges()
        for edge_id in edges:
            edge = edges.get(edge_id)
            source = edge.get("source")
            target = edge.get("target")
            if self.get_type_of_node(source) not in ["destination_node", "shortcut_node"]:
                target_nodes = connect_to(target)
                self.edges_full.extend([
                    {
                        "source": source,
                        "target": target_tmp,
                        "source_type": self.get_type_of_node(source),
                        "target_type": self.get_type_of_node(target_tmp)
                    }
                    for target_tmp in target_nodes
                ])

    def get_destination_id(self, shortcut_id):
        shortcut_node = self.get_node_by_id(shortcut_id)
        shortcut_id = shortcut_node.get("connection_id")
        destination_nodes = self.get_nodes_by_type("destination_node")
        for node_id in destination_nodes:
            node = destination_nodes.get(node_id)
            destination_id = node.get("connection_id")
            if destination_id == shortcut_id:
                return node_id
        return None


class DrawioValidator:
    def __init__(self, drawio_extractor: DrawioExtractor):
        with open(PATH_VALIDATE_FORMAT, "r") as file_in:
            self.validate_format = jsonio.load_utf8(file_in)
        with open(PATH_CONNECTION_RULE, "r") as file_in:
            self.connection_rule = jsonio.load_utf8(file_in)
            self.type_nodes = [key for key in self.connection_rule]
        self.drawio_extractor = drawio_extractor
        self.validate_logger = {
            "status": -1,
            "logs": None
        }

    def validate(self):
        logs = []
        # validate data
        for type_node in self.type_nodes:
            nodes = self.drawio_extractor.get_nodes_by_type(type_node)
            data_format = self.validate_format.get(type_node)
            for node_id in nodes:
                data = nodes.get(node_id)
                logs.extend(self.get_validate_data_result(
                    node_id,
                    data=data,
                    json_format=data_format)
                )

        # validate shortcut and destination
        logs.extend(self.get_validate_shortcut_destination())

        # validate edges
        logs.extend(self.get_validate_edges())

        # check connection
        all_nodes = self.drawio_extractor.get_nodes()
        for node_id in all_nodes:
            node = all_nodes.get(node_id)
            logs.extend(self.get_validate_connection(node))

        # check connect from action to flow
        logs.extend(self.get_validate_output_vertex())

        # check number of start_node
        all_nodes = self.drawio_extractor.get_nodes()
        count_start = 0
        for node_id in all_nodes:
            node = all_nodes.get(node_id)
            type_node = node.get("type_node")
            if type_node == "start_node":
                count_start += 1
        if count_start == 0:
            logs.append(f"[ERROR] Not found any start_node, must exist only one start_node")
        if count_start > 1:
            logs.append(f"[ERROR] Have more than one start_node, must exist only one start_node")
        # for log in logs:
        #     print(log)
        return logs

    def get_validate_data_result(self, node_id, data, json_format):
        logs = []
        if isinstance(json_format, dict):
            if not isinstance(data, dict):
                logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] {data} is not a dictionary")
                return logs
            # warning not used param
            # for param in data:
            #     if param not in json_format:
            #         logs.append(f"[WARNING] [{self.get_coordinate(node_id)}] {param} not use in {data}")
            for param in json_format:
                if param not in data:
                    logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] {param} not found in {data}")
                    return logs
                data_child = data.get(param)
                format_child = json_format.get(param)
                logs.extend(self.get_validate_data_result(node_id, data_child, format_child))
                return logs
        else:
            if json_format.startswith("text"):
                if isinstance(data, str):
                    if "text:" in json_format:
                        list_value = json_format.replace("text:", "").split(",")
                        if data.lower() not in list_value:
                            logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] "
                                        f"{data}: is wrong name, value must in {list_value}")
                    return logs
                else:
                    logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] "
                                f"{data} is not a text")
                    return logs
            elif json_format == "dict-of-text":
                if isinstance(data, dict):
                    for key in data:
                        value = data.get(key)
                        if not isinstance(value, str):
                            logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] "
                                        f"{key}:{value} is not text in dict {data}")
                            return logs
                    return logs
                else:
                    logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                                f" {data} is not a dict-of-text")
                    return logs
            elif json_format.startwiths("list-of-text"):
                len_list = json_format.split(":")[1]
                avail_len_list = len_list.split(",")
                avail_len_list = [len_list.strip() for len_list in avail_len_list]
                if isinstance(data, list):
                    for value in data:
                        if not isinstance(value, str):
                            logs.append(f"[ERROR][{self.get_coordinate(node_id)}]"
                                        f" {value} is not text in list {data}")
                            return logs
                    len_list = len(data)
                    if len_list > 1 and "n" not in avail_len_list:
                        logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                                    f" len of {data} must in {avail_len_list}")
                    if len_list == 0 and "0" not in avail_len_list:
                        logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                                    f" len of {data} must in {avail_len_list}")
                    if len_list == 1 and "1" not in avail_len_list:
                        logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                                    f" len of {data} must in {avail_len_list}")
                    return logs
                else:
                    logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                                f" {data} is not a list-of-text")
                    return logs
            else:
                if data != json_format:
                    logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                                f" {data} must be {json_format}")
                return logs

    def get_validate_shortcut_destination(self):
        logs = []
        # check duplicate destination
        destination_nodes = self.drawio_extractor.get_nodes_by_type("destination_node")
        count_destination = {}
        for node_id in destination_nodes:
            node = destination_nodes.get(node_id)
            destination_id = node.get("connection_id")
            if not destination_id:
                logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] Destination {node_id} dont have connection_id")
            else:
                if destination_id not in count_destination:
                    count_destination[destination_id] = 1
                else:
                    count_destination[destination_id] += 1
        for destination_id in count_destination:
            if count_destination[destination_id] > 1:
                logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] Duplicate destination {destination_id}")

        # check dead end shortcut
        shortcut_nodes = self.drawio_extractor.get_nodes_by_type("shortcut_node")
        for node_id in shortcut_nodes:
            node = shortcut_nodes.get(node_id)
            shortcut_id = node.get("connection_id")
            if not shortcut_id:
                logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] Destination {node_id} dont have connection_id")
            else:
                if shortcut_id not in count_destination:
                    logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] Dead-end shortcut {shortcut_id}")
        return logs

    def get_validate_edges(self):
        logs = []
        # check duplicate edges
        count_edges = {}
        edges = self.drawio_extractor.get_edges()
        for edge_id in edges:
            edge = edges.get(edge_id)
            s_t_edge = edge.get("source"), edge.get("target")
            if s_t_edge not in count_edges:
                count_edges[s_t_edge] = 1
            else:
                count_edges[s_t_edge] += 1
        for s_t_edge in count_edges:
            if count_edges[s_t_edge] > 1:
                logs.append(f"[ERROR] [{self.get_coordinate(s_t_edge[0])}] [{self.get_coordinate(s_t_edge[1])}]"
                            f" Duplicate Edge {s_t_edge}")

        # check error edges
        edges = self.drawio_extractor.get_edges()
        for edge_id in edges:
            edge = edges.get(edge_id)
            if not edge.get("source"):
                logs.append(f"[ERROR] [{self.get_coordinate(edge_id)}] Edge {edge_id} don't have source node")
            if not edge.get("target"):
                logs.append(f"[ERROR] [{self.get_coordinate(edge_id)}] Edge {edge_id} don't have target node")
            if not edge.get("source_type"):
                logs.append(f"[ERROR] [{self.get_coordinate(edge_id)}] Edge {edge_id} have a wrong format source node")
            if not edge.get("target_type"):
                logs.append(f"[ERROR] [{self.get_coordinate(edge_id)}] Edge {edge_id} have a wrong format target node")

        # check loop edges
        edges = self.drawio_extractor.get_edges()
        for edge_id in edges:
            edge = edges.get(edge_id)
            source = edge.get("source")
            target = edge.get("target")
            if source and target and source == target:
                logs.append(f"f[ERROR] [{self.get_coordinate(edge_id)}] Edge {edge_id} is a loop-edge")
        return logs

    def get_coordinate(self, node_id):
        mx_geometry = self.drawio_extractor.dict_mx_cell.get(node_id).get('mxGeometry')
        x = mx_geometry.get('x')
        y = mx_geometry.get('y')
        if not x or not y:
            x = mx_geometry.get("mxPoint")
            y = []
        try:
            desc = self.drawio_extractor.get_nodes().get(node_id).get("desc")
        except:
            desc = "N/A"
        return x, y, desc

    def get_validate_output_vertex(self):
        logs = []
        nodes = self.drawio_extractor.get_nodes()
        for node_id in nodes:
            connect_two_types = 0
            for type_node in VERTICES:
                if self.is_one_2_many(node_id, type_node):
                    connect_two_types += 1
                    break
            for type_node in FLOWS:
                if self.is_one_2_many(node_id, type_node):
                    connect_two_types += 1
                    break
            if connect_two_types > 1:
                logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                            f" {node_id} can't not connect to ACTION and FLOW at the same time")
        return logs

    def get_validate_connection(self, node):
        logs = []
        node_id = node.get("id")
        type_node = node.get("type_node")
        rule = self.connection_rule.get(type_node)

        rule_one_2_one = rule.get("one_2_one")
        for target_type in rule_one_2_one:
            if not self.is_one_2_one(node_id, target_type):
                logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                            f" {node_id} must connect to most one {target_type}")
        rule_not_connect_to = rule.get("not_connect_to")
        for target_type in rule_not_connect_to:
            if not self.is_not_connect_to(node_id, target_type):
                logs.append(f"[ERROR] [{self.get_coordinate(node_id)}]"
                            f" {node_id} must not connect to one {target_type}")
        is_open = rule.get("is_open")
        check_open = False
        for target_type in self.type_nodes:
            if self.is_one_2_many(node_id, target_type):
                check_open = True
        if is_open and not check_open:
            logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] "
                        f"{node_id} {self.get_coordinate(node_id)} must connect to at least one node")
        if not is_open and check_open:
            logs.append(f"[ERROR] [{self.get_coordinate(node_id)}] "
                        f"{node_id} {self.get_coordinate(node_id)} must not connect to any node")
        return logs

    def get_list_target_nodes(self, node_id, target_type):
        if node_id is None:
            return []
        target_nodes = []
        for edge_id in self.drawio_extractor.edges:
            edge = self.drawio_extractor.edges.get(edge_id)
            if node_id == edge.get("source"):
                target_id = edge.get("target")
                if not target_id or not edge.get("source"):
                    continue
                node_target_type = self.drawio_extractor.dict_mx_cell.get(target_id).get("type_node")

                if node_target_type == "shortcut_node":
                    destination_id = self.get_destination_id(target_id)
                    if destination_id:
                        target_nodes.extend(self.get_list_target_nodes(destination_id, target_type))

                if node_target_type == "destination_node":
                    target_nodes.extend(self.get_list_target_nodes(target_id, target_type))

                if node_target_type == target_type:
                    target_nodes.append(target_id)
        return target_nodes

    def is_one_2_one(self, node_id, target_type):
        target_nodes = self.get_list_target_nodes(node_id, target_type)
        if len(target_nodes) <= 1:
            return True
        else:
            return False

    def is_one_2_many(self, node_id, target_type):
        target_nodes = self.get_list_target_nodes(node_id, target_type)
        if len(target_nodes) > 0:
            return True
        else:
            return False

    def is_not_connect_to(self, node_id, target_type):
        target_nodes = self.get_list_target_nodes(node_id, target_type)
        if len(target_nodes) == 0:
            return True
        else:
            return False

    def get_destination_id(self, shortcut_id):
        destination_nodes = self.drawio_extractor.get_nodes_by_type("destination_node")
        for node_id in destination_nodes:
            node = destination_nodes.get(node_id)
            destination_id = node.get("connection_id")
            if destination_id == shortcut_id:
                return node_id
        return None
