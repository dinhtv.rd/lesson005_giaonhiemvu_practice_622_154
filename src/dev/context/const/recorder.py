import logging
from typing import Dict, Text, Any, List
from rasa.shared.core.trackers import DialogueStateTracker
from rasa.shared.core.events import UserUttered
import json
import copy

from src.core.condition.interfaces.impl_record import ImplUpdateSlot
from src.core.database.subscriber_store import SubscriberStore
from src.core.database.user_store import UserInfoStore
from config import (
    DEFAULT_USER_INFO
)
from src.dev.context.const.logger import GetDevLogger
from src.dev.context.entities import GetEntities
from src.dev.context.report import GetReport
from src.dev.context.define import (
    CONFIG_RECORDS,
    CONFIG_NAME_QUERY_RECORDS
)

user_store = UserInfoStore()
subscriber_store = SubscriberStore()


class Recorder:
    """
    record of conversation by sender_id
    """

    def __init__(self, sender_id) -> None:
        self.query_records = copy.deepcopy(CONFIG_NAME_QUERY_RECORDS)
        self.sender_id = sender_id
        self.state = True
        self.old_action = None
        self.pre_action = None
        self.cur_action = None
        self.cur_intent = None
        self.cur_blocks = None
        self.model_intent = None
        self.last_message = None
        self.last_image = None
        self.last_metadata = None
        self.custom_records = {}
        self.trace_nodes = []
        self.custom_records.update(copy.deepcopy(CONFIG_RECORDS))
        manual_records = {
            "get_entities": GetEntities(),
            "get_report": GetReport(),
            "get_logger": GetDevLogger(),
        }
        self.custom_records.update(manual_records)
        self.slots_record = {}
        self.user_info = DEFAULT_USER_INFO.copy()
        self.get_information_from_database(sender_id)
        self.slots_record_store = copy.deepcopy(self.slots_record)
        self.custom_records_store = copy.deepcopy(self.custom_records)
        self.old_action_store = copy.deepcopy(self.old_action)
        self.pre_action_store = copy.deepcopy(self.pre_action)
        self.cur_action_store = copy.deepcopy(self.cur_action)
        self.cur_blocks_store = copy.deepcopy(self.cur_blocks)
        self.trace_nodes_store = copy.deepcopy(self.trace_nodes)

    def get_information_from_database(self, sender_id):
        """
        get information of user from database -> set to self.user_info
        if any info is None -> set to default value from user_info.json
        :param sender_id:
        :return: None
        """
        info = user_store.get(sender_id)
        for key in info:
            value = info[key]
            if value is not None and value.lower() != "null" and value.lower() != "none":
                self.user_info[key] = value

        user_info_subscriber = self.get_info_from_subscriber()
        if user_info_subscriber:
            for key in user_info_subscriber:
                if key in self.user_info:
                    value = user_info_subscriber.get(key)
                    if value:
                        self.user_info[key] = value

    def update_from_tracker(self, tracker: DialogueStateTracker) -> None:
        """
        from tracker, update params
        self.pre_action
        self.slots_record
        :param tracker:
        :return:
        """
        self.last_message = tracker.current_state().get("latest_message").get("text")
        self.cur_intent = tracker.latest_message.parse_data["intent"]["name"]
        last_event = None
        for e in list(tracker.events):
            if isinstance(e, UserUttered):
                last_event = e
        self.last_metadata = last_event if not last_event else last_event.as_dict().get("metadata")

        current_entities = tracker.current_state().get("latest_message").get("entities")
        current_slots = {}
        for entity in current_entities:
            name_slot = entity.get("entity")
            value = entity.get("value")
            current_slots[name_slot] = value

        self.slots_record_store = copy.deepcopy(self.slots_record)
        for slot in current_slots:
            value = current_slots[slot]
            value = value if value is None else str(value)
            if slot not in self.slots_record:
                self.slots_record[slot] = []
            self.slots_record[slot].append(value)
        for slot in self.slots_record:
            if slot not in current_slots:
                self.slots_record[slot].append(None)

    def update_report_to_db(self) -> None:
        report = self.custom_records["get_report"].get_value()
        report = json.dumps(report, ensure_ascii=False)
        user_store.update_report(self.sender_id, report)

    def update_info_to_subscriber(self) -> None:
        customer_phone = self.get_user_info_record().get("customer_phone")
        if customer_phone:
            subscriber_info: Dict = self.get_current_record().get("subscriber_forward")
            if subscriber_info:
                subscriber_store.save_data(customer_phone, subscriber_info)

    def get_info_from_subscriber(self):
        customer_phone = self.get_user_info_record().get("customer_phone")
        if customer_phone:
            return subscriber_store.get_data(customer_phone)
        else:
            return None

    def get_entities(self):
        entities = self.get_current_record().get("get_entities")
        return {} if not entities else entities

    def get_entities_dumped(self):
        return json.dumps(self.get_entities(), ensure_ascii=False)

    def update_trace_node(self, node_id):
        self.trace_nodes.append(node_id)
        self.custom_records["repeat_count"].update(self)

    def update_by_condition_id(self, condition_id, args=None):
        self.custom_records[condition_id].update(self, args)

    def reset_update_local(self):
        for condition_id in self.custom_records:
            if self.custom_records[condition_id].type() == "update_local":
                self.custom_records[condition_id].reset()

    def update_checkpoint(self):
        self.custom_records["checkpoint"].update(self)

    def update(self, tracker: DialogueStateTracker,
               cur_action: Text,
               cur_intent: Text,
               cur_blocks: List,
               model_intent=None,
               update_record=True,
               condition_ids=None) -> None:
        """"""
        self.cur_intent = copy.deepcopy(cur_intent)
        if cur_intent is not None:
            self.old_action = copy.deepcopy(self.pre_action)
            self.pre_action = copy.deepcopy(self.cur_action)
            self.cur_action = copy.deepcopy(cur_action)
            self.cur_blocks = copy.deepcopy(cur_blocks)
            self.model_intent = copy.deepcopy(model_intent)
        self.update_from_tracker(tracker)
        if update_record:
            self.update_records(condition_ids)

    def update_records(self, condition_ids=None):
        dict_conditions = {}
        for condition_id, args in condition_ids:
            dict_conditions[condition_id] = args
        for name_record in self.custom_records:
            if name_record not in self.query_records and \
                    (condition_ids is None or name_record in dict_conditions or isinstance(
                        self.custom_records[name_record], ImplUpdateSlot)
                     or name_record in ["get_report", "get_entities", "get_logger"]):
                import logging
                self.custom_records[name_record].update(self, dict_conditions.get(name_record))

    async def update_async_method(self):
        for name_record in self.query_records:
            if name_record in self.custom_records:
                await self.custom_records[name_record].update(self)

    def store(self):
        """
        store value of some attributes of recorder
        :return: None
        """
        self.slots_record_store = copy.deepcopy(self.slots_record)
        self.custom_records_store = copy.deepcopy(self.custom_records)
        self.old_action_store = copy.deepcopy(self.old_action)
        self.pre_action_store = copy.deepcopy(self.pre_action)
        self.cur_action_store = copy.deepcopy(self.cur_action)
        self.cur_blocks_store = copy.deepcopy(self.cur_blocks)
        self.trace_nodes_store = copy.deepcopy(self.trace_nodes)

    def roll_back(self):
        """
        Rollback from stored-data
        :return: None
        """
        self.slots_record = copy.deepcopy(self.slots_record_store)
        self.custom_records = copy.deepcopy(self.custom_records_store)
        self.old_action = copy.deepcopy(self.old_action_store)
        self.pre_action = copy.deepcopy(self.pre_action_store)
        self.cur_action = copy.deepcopy(self.cur_action_store)
        self.cur_blocks = copy.deepcopy(self.cur_blocks_store)
        self.trace_nodes = copy.deepcopy(self.trace_nodes_store)

    def get_last_metadata(self):
        """
        Last metadata from user
        :return: last_metadata
        """
        return self.last_metadata

    def get_last_message(self):
        """
        Last message from user
        :return: last_message
        """
        return self.last_message

    def get_last_slot_receive(self, name: Text):
        """
        get slot value from last message received
        :param name: name of slot
        :return:value of slot
        """
        if name not in self.slots_record:
            return None
        return self.slots_record[name][-1]

    def get_last_slot_value(self, name: Text):
        """
        get last slot value not None by tracking
        :param name: naoe of slot
        :return:value of slot
        """
        if name not in self.slots_record:
            return None
        for value in reversed(self.slots_record[name]):
            if value is not None:
                return value
        return None

    def get_current_record(self) -> Dict[Text, Any]:
        """
        get value of record
        :return: dictionary of record items
        """
        result = self.user_info.copy()
        for name_record in self.custom_records:
            result[name_record] = self.custom_records[name_record].get_value()
        return result

    def get_user_info_record(self) -> Dict[Text, Any]:
        """
        get value of record
        :return: dictionary of record items
        """
        result = self.user_info
        return result

    @classmethod
    def create_recorder(cls,
                        sender_id,
                        old_action,
                        pre_action,
                        cur_action,
                        cur_blocks,
                        trace_nodes,
                        cur_intent,
                        model_intent,
                        last_message,
                        last_image,
                        last_metadata,
                        custom_records,
                        slots_record,
                        user_info,
                        state
                        ):
        """
        Remake Recorder from value of all attributes
        :param sender_id:
        :param old_action:
        :param pre_action:
        :param cur_action:
        :param cur_blocks:
        :param trace_nodes:
        :param cur_intent:
        :param model_intent:
        :param last_message:
        :param last_image:
        :param last_metadata:
        :param custom_records:
        :param slots_record:
        :param user_info:
        :param state:
        :return:
        """
        recorder = cls(None)
        recorder.sender_id = sender_id
        recorder.old_action = old_action
        recorder.pre_action = pre_action
        recorder.cur_action = cur_action
        recorder.cur_blocks = cur_blocks
        recorder.trace_nodes = trace_nodes
        recorder.cur_intent = cur_intent
        recorder.model_intent = model_intent
        recorder.last_message = last_message
        recorder.last_image = last_image
        recorder.last_metadata = last_metadata
        for key in custom_records:
            recorder.custom_records[key].set_record(custom_records[key])
        recorder.slots_record = slots_record
        recorder.user_info = user_info
        recorder.state = state
        recorder.slots_record_store = copy.deepcopy(recorder.slots_record)
        recorder.custom_records_store = copy.deepcopy(recorder.custom_records)
        recorder.old_action_store = copy.deepcopy(recorder.old_action)
        recorder.pre_action_store = copy.deepcopy(recorder.pre_action)
        recorder.cur_action_store = copy.deepcopy(recorder.cur_action)
        recorder.cur_blocks_store = copy.deepcopy(recorder.cur_blocks)
        recorder.trace_nodes_store = copy.deepcopy(recorder.trace_nodes)
        return recorder

    @classmethod
    def from_dict(cls, conversation_id, data):
        """
        Create new Recorder from dict-data
        :param conversation_id: conversation id
        :param data: dict data
        :return: new Recorder
        """
        recorder = Recorder.create_recorder(
            sender_id=conversation_id,
            old_action=data["old_action"],
            pre_action=data["pre_action"],
            cur_action=data["cur_action"],
            cur_blocks=data["cur_blocks"],
            trace_nodes=data["trace_nodes"],
            cur_intent=data["cur_intent"],
            model_intent=data["model_intent"],
            last_message=data["last_message"],
            last_image=data["last_image"],
            last_metadata=data["last_metadata"],
            custom_records=data["custom_records"],
            slots_record=data["slots_record"],
            user_info=data["user_info"],
            state=data["state"]
        )
        return recorder

    def dumps(self):
        """
        Dump self - Recorder to dict data
        :return: dict of attributes of Recorder
        """
        data = {}
        data["old_action"] = self.old_action
        data["pre_action"] = self.pre_action
        data["cur_action"] = self.cur_action
        data["cur_blocks"] = self.cur_blocks
        data["trace_nodes"] = self.trace_nodes
        data["cur_intent"] = self.cur_intent
        data["model_intent"] = self.model_intent
        data["last_message"] = self.last_message
        data["last_image"] = self.last_image
        data["last_metadata"] = self.last_metadata
        custom_records = {}
        for key in self.custom_records:
            custom_records[key] = self.custom_records[key].get_value()
        data["custom_records"] = custom_records
        data["slots_record"] = self.slots_record
        data["user_info"] = self.user_info
        data["state"] = self.state
        return data
