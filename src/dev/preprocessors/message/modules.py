import re
import json
from flashtext import KeywordProcessor

from src.dev.preprocessors.message.impl_message_module import ImplMessageModule
from src.dev.preprocessors.message.vietnamese_norm import norm_text

from config import (
    PATH_REPLACEMENT_DICTIONARY
)


class LowerCase(ImplMessageModule):
    def __init__(self):
        super().__init__()

    def process(self, message):
        return message.lower()


class RemoveSequentZeros(ImplMessageModule):
    def __init__(self):
        super().__init__()

    def process(self, message):
        max_sequent = 10
        for len_sequent in range(3, max_sequent):
            zeros = "0" * len_sequent
            zeros_txt = "không " * len_sequent
            zeros_txt = zeros_txt[:-1]
            pattern_sequent_zeros = f"\\b{zeros}\\b"
            message = re.sub(pattern_sequent_zeros, zeros_txt, message)
        return message


class ReplaceWithDictionary(ImplMessageModule):
    def __init__(self):
        super().__init__()
        with open(PATH_REPLACEMENT_DICTIONARY, "r") as file_dictionary:
            self.REPLACEMENT_DICTIONARY = json.load(file_dictionary)

    def process(self, message):
        keyword_processor = KeywordProcessor(case_sensitive=False)
        keyword_processor.add_keywords_from_dict(self.REPLACEMENT_DICTIONARY)
        new_message = keyword_processor.replace_keywords(message)
        return new_message


class SuperNormMessage(ImplMessageModule):
    def __init__(self):
        super().__init__()
        pass

    def process(self, message):
        text_norm = norm_text(message)
        if text_norm:
            return text_norm
        else:
            return message
