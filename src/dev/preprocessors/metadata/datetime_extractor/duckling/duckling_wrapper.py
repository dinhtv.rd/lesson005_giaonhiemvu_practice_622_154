import time
import json
import logging
import os
import requests
from typing import Any, List, Optional, Text, Dict


logger = logging.getLogger(__name__)
os.environ['TZ'] = 'Asia/Ho_Chi_Minh'
time.tzset()

ENTITY_ATTRIBUTE_TYPE = "entity"


def extract_value(match: Dict[Text, Any]) -> Dict[Text, Any]:
    if match["value"].get("type") == "interval":
        value = {
            "to": match["value"].get("to", {}).get("value"),
            "from": match["value"].get("from", {}).get("value"),
        }
    else:
        value = match["value"].get("value")

    return value


def convert_duckling_format_to_rasa(
        matches: List[Dict[Text, Any]]
) -> List[Dict[Text, Any]]:
    extracted = []

    for match in matches:
        value = extract_value(match)
        entity = {
            "start": match["start"],
            "end": match["end"],
            "text": match.get("body", match.get("text", None)),
            "value": value,
            "confidence": 1.0,
            "additional_info": match["value"],
            "entity": match["dim"],
        }

        extracted.append(entity)

    return extracted


class DucklingWrapper(object):
    """Searches for structured entites, e.g. dates, using a duckling server."""

    defaults = {
        # by default all dimensions recognized by duckling are returned
        # dimensions can be configured to contain an array of strings
        # with the names of the dimensions to filter for
        "dimensions": None,
        # http url of the running duckling server
        "url": None,
        # locale - if not set, we will use the language of the model
        "locale": None,
        # timezone like Europe/Berlin
        # if not set the default timezone of Duckling is going to be used
        "timezone": None,
        # Timeout for receiving response from http url of the running duckling server
        # if not set the default timeout of duckling http url is set to 3 seconds.
        "timeout": 3,
    }

    def __init__(self, config) -> None:
        logger.debug(f"cxxxxxxxxxxxx {config}")
        self.duckling_config = config.get("duckling")
        if self.duckling_config:
            self.locale = self.duckling_config.get("locale")
            self.url = self.duckling_config.get("url")
            self.dimensions = self.duckling_config.get("dimensions")
            self.timezone = self.duckling_config.get("timezone")
            self.timeout = self.duckling_config.get("timeout")
        else:
            self.locale = None
            self.url = None
            self.dimensions = None
            self.timezone = None
            self.timeout = None

    def _payload(self, text: Text, reference_time: int) -> Dict[Text, Any]:
        return {
            "text": text,
            "locale": self.locale,
            "tz": self.timezone,
            "dims": json.dumps(self.dimensions),
            "reftime": reference_time,
        }

    async def _duckling_parse(self, text: Text, reference_time: int) -> List[Dict[Text, Any]]:
        """Sends the request to the duckling server and parses the result.

        Args:
            text: Text for duckling server to parse.
            reference_time: Reference time in milliseconds.

        Returns:
            JSON response from duckling server with parse data.
        """
        parse_url = f"{self.url}/parse"
        try:
            payload = self._payload(text, reference_time)
            headers = {
                "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
            }
            response = requests.post(
                parse_url,
                data=payload,
                headers=headers,
                timeout=self.timeout,
            )
            if response.status_code == 200:
                return response.json()
            else:
                logger.error(
                    f"Failed to get a proper response from remote "
                    f"duckling at '{parse_url}. Status Code: {response.status_code}. Response: {response.text}"
                )
                return []
        except (
                requests.exceptions.ConnectionError,
                requests.exceptions.ReadTimeout,
        ) as e:
            logger.error(
                "Failed to connect to duckling http server. Make sure "
                "the duckling server is running/healthy/not stale and the proper host "
                "and port are set in the configuration. More "
                "information on how to run the server can be found on "
                "github: "
                "https://github.com/facebook/duckling#quickstart "
                "Error: {}".format(e)
            )
            return []

    @staticmethod
    def filter_irrelevant_entities(extracted: list, requested_dimensions: set) -> list:
        """Only return dimensions the user configured."""
        if requested_dimensions:
            return [
                entity
                for entity in extracted
                if entity[ENTITY_ATTRIBUTE_TYPE] in requested_dimensions
            ]
        return extracted

    async def extract(self, message: Text):
        if self.url is not None:
            reference_time = round(time.time() * 1000)
            matches = await self._duckling_parse(message, reference_time)
            all_extracted = convert_duckling_format_to_rasa(matches)
            dimensions = self.dimensions
            extracted = DucklingWrapper.filter_irrelevant_entities(
                all_extracted, dimensions
            )
        else:
            extracted = []
            logger.warning("Duckling URL wasn't config in config/config.json")
        return extracted
