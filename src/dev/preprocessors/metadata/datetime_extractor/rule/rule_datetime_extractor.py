import re
import json
from vietnam_number import w2n
from datetime import datetime, timedelta

from src.dev.preprocessors.metadata.datetime_extractor.define import (
    DATETIME_FORMAT,
    DATE_FORMAT,
    TIME_FORMAT,
    BACKWARD,
    PATH_RULE_DATETIME_SIGNAL,
    MAX_DAYS,
)
from src.dev.preprocessors.metadata.datetime_extractor.rule.rule_datetime_regex import RuleDatetimeRegex

from src.dev.preprocessors.metadata.datetime_extractor.selector import Selector
from src.dev.preprocessors.metadata.datetime_extractor.utils.lunar_date import S2L, L2S


def norm_space(text):
    while "  " in text:
        text = text.replace("  ", " ")
    return text.strip()


def check_signal_pattern(text, patterns):
    for pattern in patterns:
        if re.search(pattern, text, re.IGNORECASE):
            return True
    return False


def get_mapping_value(text, values):
    result = None
    result_key = None
    for value in values:
        if re.search(f"\\b{value}\\b", text, re.IGNORECASE):
            tmp = values.get(value)
            if result_key is None or (isinstance(result_key, str) and len(result_key) < len(value)):
                result = tmp
                result_key = value
    return result


class RuleDatetimeExtractor(object):
    def __init__(self,
                 datetime_format=None,
                 date_format=None,
                 time_format=None) -> None:
        self.datetime_format = datetime_format if datetime_format else DATETIME_FORMAT
        self.date_format = date_format if date_format else DATE_FORMAT
        self.time_format = time_format if time_format else TIME_FORMAT
        with open(PATH_RULE_DATETIME_SIGNAL, encoding="utf8") as file_signal:
            self.signal = json.load(file_signal)
        self.regex = RuleDatetimeRegex()
        self.extractor_name = "Rule"
        self.selector = Selector()

    def normalize_number(self, number_w):
        try:
            return str(w2n(number_w))
        except:
            return str(number_w)

    async def process(self,
                      message,
                      date_from=None,
                      date_to=None,
                      time_from=None,
                      time_to=None,
                      priority_direction="backward",
                      accept_only_number=False):

        result = {
            "date": None,
            "time": None,
            "range": None,
            "values": [],
            "status_date": 400,
            "status_time": 400
        }

        result_date = self.process_date(message,
                                        date_from,
                                        date_to,
                                        priority_direction,
                                        accept_only_number)
        result_time = self.process_time(message,
                                        time_from,
                                        time_to,
                                        priority_direction,
                                        accept_only_number)
        if result_date:
            date = result_date.get("date")
        else:
            date = None
        date = self.selector.better_date(
            date_old=None,
            date_new=date,
            date_from=date_from,
            date_to=date_to,
            priority_direction=priority_direction,
            status_old=result["status_date"]
        )
        time = self.selector.better_time(
            time_old=None,
            time_new=result_time,
            time_from=time_from,
            time_to=time_to,
            priority_direction=priority_direction,
            status_old=result["status_time"]
        )

        result = {
            "date": date.get("value"),
            "time": time.get("value"),
            "range": None,
            "values": [{'type': 'date', 'value': date}, {'type': 'time', 'value': result_time}],
            "extractor": self.extractor_name,
            "status_date": date.get("status"),
            "status_time": time.get("status")
        }

        return result

    def process_date(self,
                     message,
                     date_from,
                     date_to,
                     priority_direction,
                     accept_only_number):
        regex_result = self.regex.process(message)

        regex_date = regex_result.get(self.regex.DATE)
        regex_time = regex_result.get(self.regex.TIME)
        regex_session = regex_result.get(self.regex.SESSION)
        regex_number = regex_result.get(self.regex.NUMBER)

        if regex_date:
            return self.normalize_date(raw_date=regex_date,
                                       priority_direction=priority_direction)
        else:
            if accept_only_number:
                if regex_time is None and regex_number is not None:
                    return self.normalize_date(raw_date=f"ngày {self.normalize_number(regex_number)}",
                                               priority_direction=priority_direction)
            else:
                return None

    def process_time(self,
                     message,
                     time_from,
                     time_to,
                     priority_direction,
                     accept_only_number):

        regex_result = self.regex.process(message)

        regex_date = regex_result.get(self.regex.DATE)
        regex_time = regex_result.get(self.regex.TIME)
        regex_session = regex_result.get(self.regex.SESSION)
        regex_number = regex_result.get(self.regex.NUMBER)
        if regex_time:
            if regex_session:
                return self.normalize_time(raw_time=regex_time,
                                           raw_session=regex_session)
            return self.normalize_time(raw_time=regex_time)
        else:
            if regex_session:
                return self.normalize_time(raw_session=regex_session)
            if regex_number and accept_only_number:
                return self.normalize_time(raw_time=f"{self.normalize_number(regex_number)} giờ")
        return None

    def lunar_to_sun(self, lunar_date, priority_direction, tet=False):
        normed_date = self.normalize_date(lunar_date, priority_direction)
        _date_norm = normed_date.get("date")
        if _date_norm is None:
            return lunar_date
        dt = datetime.strptime(_date_norm, DATE_FORMAT)
        lunarD = dt.day
        lunarM = dt.month if not tet else 12 if lunarD >= 20 else 1
        today = datetime.today()
        _today_moon = S2L(today.day, today.month, today.year)
        todayM = _today_moon[1]
        lunarY = _today_moon[2]
        if todayM > lunarM:
            lunarY += 1
        _date_sun = L2S(lunarD=lunarD, lunarM=lunarM, lunarY=lunarY, lunarLeap=0)
        sunD = _date_sun[0]
        sunM = _date_sun[1]
        sunY = _date_sun[2]
        dt = datetime(year=sunY, month=sunM, day=sunD)
        return dt.strftime(DATE_FORMAT)

    def normalize_lunar_date(self, lunar_date, priority_direction):
        new_date = lunar_date
        is_tet = re.search("\\btết\\b", new_date)
        replace_signal = self.get_signal_replace("lunar_date")
        for key in replace_signal:
            value = replace_signal.get(key)
            new_date = new_date.replace(key, value)
        new_date = norm_space(new_date)
        return {
            "date": self.lunar_to_sun(new_date, priority_direction, tet=is_tet),
            "text_repeat": lunar_date
        }

    def get_signal_patterns(self, name):
        return self.signal.get("signal").get(name).get("patterns")

    def get_signal_replace(self, name):
        return self.signal.get("signal").get(name).get("replace")

    def get_signal_values(self, name):
        return self.signal.get("signal").get(name).get("values")

    def normalize_sun_date(self, sun_date, priority_direction):
        sun_date, is_rep_1 = self.normalize_speech_date(sun_date, priority_direction)
        sun_date, is_rep_2 = self.normalize_format_date(sun_date, priority_direction)
        is_rep = is_rep_1 or is_rep_2
        return {
            "date": sun_date,
            "is_repeat": is_rep
        }

    def normalize_format_date(self, sun_date, priority_direction):
        sun_date = sun_date.replace("mười một", "11")
        sun_date = sun_date.replace("mười hai", "12")
        words = sun_date.replace("/", " ").split(" ")
        today = datetime.today()
        today_day, today_month, today_year = today.day, today.month, today.year
        new_date = ""
        for word in words:
            word = self.normalize_number(word)
            if not word.isdigit():
                word = " "
            new_date = new_date + word + " "
        new_date = norm_space(new_date)
        if new_date == "":
            date_items = []
        else:
            date_items = new_date.split(" ")
        if len(date_items) == 1:
            day = int(self.normalize_number(date_items[0]))
            month = today_month
            year = today_year
            if priority_direction == BACKWARD:
                if day > today_day:
                    month -= 1
                if month < 0:
                    month = 12
                    year -= 1
            else:
                if day < today_day:
                    month += 1
                if month > 12:
                    month = 1
                    year += 1
        elif len(date_items) == 2:
            day = int(self.normalize_number(date_items[0]))
            month = int(self.normalize_number(date_items[1]))
            year = today_year
            if priority_direction == BACKWARD:
                if month > today_month:
                    year -= 1
            else:
                if month < today_month:
                    year += 1
        elif len(date_items) == 3:
            day = int(self.normalize_number(date_items[0]))
            month = int(self.normalize_number(date_items[1]))
            year = int(self.normalize_number(date_items[2]))
        else:
            return None, False
        new_date = datetime(
            day=day,
            month=month,
            year=year
        )
        new_date = f"{new_date.strftime('%d/%m/%Y')}"
        return new_date, False

    def normalize_speech_date(self, sun_date, priority_direction):
        today = datetime.today()
        today_day, today_month, today_year = today.day, today.month, today.year
        for pattern in self.get_signal_patterns("next_month"):
            if re.search(pattern, sun_date):
                sun_date = re.sub(pattern, f"tháng {today_month + 1}", sun_date)

        for pattern in self.get_signal_patterns("prev_month"):
            if re.search(pattern, sun_date):
                sun_date = re.sub(pattern, f"tháng {today_month + 1}", sun_date)

        is_range_day = check_signal_pattern(sun_date, self.get_signal_patterns("range_day"))
        is_next_day = not is_range_day and check_signal_pattern(sun_date, self.get_signal_patterns("next_day"))
        is_prev_day = not is_range_day and not is_next_day and \
                      check_signal_pattern(sun_date, self.get_signal_patterns("prev_day"))
        is_weekday = check_signal_pattern(sun_date, self.get_signal_patterns("weekday"))
        is_next_week = is_weekday and check_signal_pattern(sun_date, self.get_signal_patterns("next_week"))
        is_last_week = is_weekday and (not is_next_week) and \
                       check_signal_pattern(sun_date, self.get_signal_patterns("last_week"))

        range_days = MAX_DAYS
        if is_range_day:
            previous_pattern = "\\b(ngày|hôm|bữa) trước\\b"
            next_pattern = "\\b(hôm|ngày|bữa) (nữa|sau|tới|kế|tiếp)\\b"
            if re.search(previous_pattern, sun_date):
                range_days = -int(self.normalize_number(norm_space(re.sub(previous_pattern, "", sun_date))))
            if re.search(next_pattern, sun_date):
                range_days = int(self.normalize_number(norm_space(re.sub(next_pattern, "", sun_date))))
        if is_next_day:
            range_days = get_mapping_value(sun_date, self.get_signal_values("next_day"))
        if is_prev_day:
            range_days = get_mapping_value(sun_date, self.get_signal_values("prev_day"))

        if is_weekday:
            weekdays = self.get_signal_values("weekday")
            if is_last_week:
                for pattern in self.get_signal_patterns("last_week"):
                    sun_date = re.sub(pattern, "", sun_date)
                    sun_date = norm_space(sun_date)
                range_days_to_sunday = (7 + datetime.today().weekday() - weekdays["chủ nhật"]) % 7
                sunday_to_date = (7 + weekdays["chủ nhật"] - weekdays[sun_date]) % 7
                range_days = - (range_days_to_sunday + sunday_to_date)
            elif is_next_week:
                for pattern in self.get_signal_patterns("next_week"):
                    sun_date = re.sub(pattern, "", sun_date)
                    sun_date = norm_space(sun_date)
                range_days_to_sunday = (7 + weekdays["chủ nhật"] - datetime.today().weekday()) % 7
                sunday_to_date = (7 + weekdays[sun_date] - weekdays["chủ nhật"]) % 7
                if sunday_to_date == 0:
                    sunday_to_date = 7
                range_days = range_days_to_sunday + sunday_to_date
            elif priority_direction == BACKWARD:
                range_days = (7 + datetime.today().weekday() - weekdays[sun_date]) % 7
                range_days = -range_days
            else:
                range_days = (7 + weekdays[sun_date] - datetime.today().weekday()) % 7
        if range_days != MAX_DAYS:
            if range_days >= 0:
                new_date = datetime.today() + timedelta(days=range_days)
            else:
                new_date = datetime.today() - timedelta(days=-range_days)
            sun_date = f"{new_date.strftime('%d/%m/%Y')}"
            return sun_date, True
        return sun_date, False

    def normalize_date(self, raw_date, priority_direction):
        if raw_date:
            is_lunar_date = check_signal_pattern(raw_date, self.get_signal_patterns("lunar_date"))
            if is_lunar_date:
                return self.normalize_lunar_date(raw_date, priority_direction)
            return self.normalize_sun_date(raw_date, priority_direction)
        else:
            return None

    def normalize_time(self, raw_time=None, raw_session=None):
        hour = None
        minute = None
        if raw_time:
            print(raw_time)
            replace_signal = self.get_signal_replace("speech_time")
            for key in replace_signal:
                value = replace_signal.get(key)
                raw_time = raw_time.replace(key, value)
            while "h0" in raw_time:
                raw_time = raw_time.replace("h0", "h")
            raw_time = norm_space(raw_time)
            if raw_time.endswith("h"):
                raw_time = raw_time + "0"
            raw_time = raw_time.replace(" ", "")
            time_items = raw_time.split("h")
            hour = int(self.normalize_number(time_items[0]))
            minute = int(self.normalize_number(time_items[1]))

            if raw_session:
                is_first_haft_day = hour < 12 or (hour == 12 and minute == 0)
                if is_first_haft_day:
                    if "trưa" in raw_session:
                        if hour == 1 or hour == 2:
                            hour += 12
                    elif "chiều" in raw_session:
                        hour += 12
                    elif "tối" in raw_session or "đêm" in raw_session and hour > 4:
                        hour += 12
            if minute > 60:
                minute = 0
        elif raw_session:
            is_session = check_signal_pattern(raw_session, self.get_signal_patterns("session"))
            if is_session:
                value = get_mapping_value(raw_session, self.get_signal_values("session"))
                if value:
                    hour = value.get("hour")
                    minute = value.get("minute")
        is_valid = hour is not None and minute is not None
        if is_valid:
            today = datetime.today()
            time_datetime = datetime(today.year, today.month, today.day, hour=hour, minute=minute)
            time_format = time_datetime.strftime(self.time_format)
            return time_format
        return None
