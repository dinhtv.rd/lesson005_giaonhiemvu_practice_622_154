from typing import Any, Text
import logging

from src.dev.preprocessors.metadata.datetime_extractor.general_datetime_extractor import GeneralDatetimeExtractor
from src.dev.preprocessors.metadata.impl_metadata_module import ImplExternalModule
from src.dev.preprocessors.metadata.define import (
    ENDPOINT_NER_PERSONAL_NAME,
    ENDPOINT_NER_ADDRESS,
    ACCEPT_ACTIONS_FOR_DATETIME_VALUE
)
from src.core.utils.requests.async_request import json_async_request

from src.core.utils.io import jsonio

logger = logging.getLogger(__name__)


class NERPersonalName(ImplExternalModule):
    def __init__(self):
        super().__init__()

    def name(self):
        return "ner_personal_name"

    async def process(self, message: Text, recorder=None) -> Any:
        # if not recorder or recorder.cur_action not in ACCEPT_ACTIONS_FOR_NER_PERSONAL_NAME:
        #     return None
        url = ENDPOINT_NER_PERSONAL_NAME
        payload = {
            "text": message,
            "name_entity": None
        }
        payload = jsonio.dumps_utf8(payload)
        headers = {
            'Content-Type': 'application/json'
        }
        response = await json_async_request(url=url, payload=payload, headers=headers, method="POST")
        status = response.get("status")
        result = response.get("result")
        if status is None or status != 200:
            result = None
        else:
            result = result.get("result")
        return result


class NERAddress(ImplExternalModule):
    def __init__(self):
        super().__init__()
    #     self.extractor = self.load()

    # def load(self):
    #     regex_process_entity = RegexProcessEntity(PATH_JSON_REGEX_FEATURE_ADDRESS, DIR_REGEX_ADDRESS)
    #     with open(PATH_TEXT2NUM, "r", encoding='utf-8') as file:
    #         text2number = jsonio.load_utf8(file)
    #     norm_number = NormalizeNumber(regex_feature=regex_process_entity, text2number=text2number)
    #     extractor_layer_address = ExtractorLayerAddress(regex_process_entity,
    #                                                     norm_number,
    #                                                     dir_address=DIR_REGEX_ADDRESS)
    #     return extractor_layer_address

    def name(self):
        return "ner_address"

    async def process(self, message: Text, recorder=None) -> Any:
        # if not recorder or recorder.cur_action not in ACCEPT_ACTIONS_FOR_NER_ADDRESS:
        #     return None
        url = ENDPOINT_NER_ADDRESS
        payload = {
            "text": message,
            "name_entity": None
        }
        payload = jsonio.dumps_utf8(payload)
        headers = {
            'Content-Type': 'application/json'
        }
        response = await json_async_request(url=url, payload=payload, headers=headers, method="POST")
        status = response.get("status")
        result = response.get("result")
        if status is None or status != 200:
            result = None
        else:
            result = result.get("result")
        return result


class DatetimeExtractor(ImplExternalModule):
    def __init__(self):
        super().__init__()
        self.extractor = GeneralDatetimeExtractor()
        self.time_from = None
        self.time_to = None
        self.priority_direction = "FORWARD"

    def name(self):
        return "datetime_value"

    async def process(self, message: Text, recorder=None) -> Any:
        if not recorder or recorder.cur_action not in ACCEPT_ACTIONS_FOR_DATETIME_VALUE or not message:
            return None
        try:
            answer = await self.extractor.process(message, self.time_from, self.time_to, self.priority_direction)
        except:
            answer = None
        return answer
